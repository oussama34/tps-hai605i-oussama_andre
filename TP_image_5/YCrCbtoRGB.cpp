#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue1[256], cNomImgLue2[256], cNomImgLue3[256], cNomImgEcrite[256];
  int nH, nW, nTaille, nY, nCb, nCr;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn1.pgm ImageIn2.ppm ImageIn3.pgm ImageOut.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue1);
   sscanf (argv[2],"%s",cNomImgLue2);
   sscanf (argv[3],"%s",cNomImgLue3);
   sscanf (argv[4],"%s",cNomImgEcrite);

   OCTET *ImgIn1, *ImgIn2, *ImgIn3, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn1, OCTET, nTaille);
   lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);
   
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   
   allocation_tableau(ImgIn3, OCTET, nTaille);
   lire_image_pgm(cNomImgLue3, ImgIn3, nH * nW);
   
   allocation_tableau(ImgOut, OCTET, nTaille3);
   


   int j = 0;
   for (int i = 0; i < nTaille3; i+=3)
     {
       nY = ImgIn1[j];
       nCb = ImgIn2[j];
       nCr = ImgIn3[j];
       j++;
       ImgOut[i] = nY + 1.402*(nCr-128);
       ImgOut[i+1] = nY - 0.34414*(nCb - 128) - 0.714414*(nCr - 128);
       ImgOut[i+2] = nY + 1.772 * (nCb - 128);
     }
	  

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn1); free(ImgIn2); free(ImgIn3); free(ImgOut);
   return 1;
}
