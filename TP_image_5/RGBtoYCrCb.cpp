#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite[256], cNomImgEcrite1[256], cNomImgEcrite2[256], cNomImgEcrite3[256];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 6) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm ImageOut1.ppm ImageOut2.ppm ImageOut3.ppm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%s",cNomImgEcrite1);
   sscanf (argv[4],"%s",cNomImgEcrite2);
   sscanf (argv[5],"%s",cNomImgEcrite3);
   
   OCTET *ImgIn, *ImgOut ,*ImgOut1, *ImgOut2, *ImgOut3;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);
   allocation_tableau(ImgOut1, OCTET, nTaille);
   allocation_tableau(ImgOut2, OCTET, nTaille);
   allocation_tableau(ImgOut3, OCTET, nTaille);
   
   int j = 0;
   for (int i = 0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       ImgOut[i] = 0.299*nR + 0.587*nG + 0.114*nB;
       ImgOut[i+1] = -0.1687*nR - 0.3313*nG + 0.5*nB + 128;
       ImgOut[i+2] = 0.5*nR - 0.4187*nG - 0.0813*nB + 128;
       ImgOut1[j] = 0.299*nR + 0.587*nG + 0.114*nB;
       ImgOut2[j] = -0.1687*nR - 0.3313*nG + 0.5*nB + 128;
       ImgOut3[j] = 0.5*nR - 0.4187*nG - 0.0813*nB + 128;
       j++;
     }

   
   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite1, ImgOut1,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite2, ImgOut2,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite3, ImgOut3,  nH, nW);
  
   free(ImgIn); free(ImgOut); free(ImgOut1); free(ImgOut2); free(ImgOut3);
   
   return 1;
}
