#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite[256];
  int nH, nW, nTaille, nY, k;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm  ImageOut.pgm k \n"); 
       exit (1) ;
     }
   
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  sscanf (argv[3],"%d",&k);

  printf("k = %d\n",k);
  if ((k >= 128) or (k <= -128)) {
    printf("Erreur : k doit etre compris entre -128 et 128\n");
    exit(2);
  }
  
  OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
   
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);  
   allocation_tableau(ImgOut, OCTET, nTaille);
   
   for (int i = 0; i < nH; i++)
     for(int j = 0; j < nW; j++)
       {
	 nY = ImgIn[i*nW+j];
	 int res = nY + k;
	 if (res > 255) { res = res - 255 ;}
	 else if (res < 0) { res = res + 255; }
	 ImgOut[i*nW+j] = res;
     }
	  

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
   return 1;
}
