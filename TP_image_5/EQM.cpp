// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgLue2[256];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageIn2.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgLue2);

   OCTET *ImgIn, *ImgIn2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);

   float EQM = 0.0;
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
	 EQM = EQM + pow((ImgIn[i*nW+j] - ImgIn2[i*nW+j]), 2);	 
       }

   EQM = EQM * (1.f/(float)(nH*nW));
   
   printf("EQM = %f\n", EQM);
   
   free(ImgIn); free(ImgIn2);

   return 1;
}
