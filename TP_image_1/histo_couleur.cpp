
#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256];
  int nH, nW, nTaille, nR, nG, nB;

  if (argc != 2)
    {
      printf("Usage: ImageIn.pgm\n");
      exit (1) ;
    }

  sscanf (argv[1],"%s",cNomImgLue);
  
  OCTET *ImgIn;
 
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;

  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgIn, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

  int tabR[256];
  for (int i=0; i < 256; i++) {
    tabR[i] = 0;
  }

  int tabG[256];
  for (int i=0; i < 256; i++) {
    tabG[i] = 0;
  }
  
  int tabB[256];
  for (int i=0; i < 256; i++) {
    tabB[i] = 0;
  }
  
  

   for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       tabR[nR] = tabR[nR] + 1;
       tabG[nG] = tabG[nG] + 1;
       tabB[nB] = tabB[nB] + 1;
     }

  FILE* fichier = NULL;
  fichier = fopen("histo_couleur.dat","w");
  if (fichier!=NULL)
    {
      for(int i=0; i < 256; i++) {
	fprintf(fichier, "%d %d %d %d\n", i, tabR[i], tabG[i], tabB[i]);
	  }
      
      fclose(fichier);
    }
  else
    {
      printf("Impossible d'ouvrir le fichier histo_couleur.dat");
    }
 
  free(ImgIn);
  return 1;
}
  
