#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  if (argc != 4)
    {
      printf("Usage: ImageIn.pgm l(pour ligne) ou c(pour colonne) numeroDeLaLigneOuColonne\n");
      exit(1);
    }
  char cNomImgLue[255], info;
  int nH, nW, nTaille, numero;

  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",&info);
  sscanf (argv[3],"%d",&numero);

  OCTET *ImgIn;
  
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  
  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

  if (info!='c' and info!='l')
    {
      printf(" le deuxieme argument est soit l pour ligne soit c pour colonne");
      exit(2);
    }
  
  int tab[255];
  for (int i=0; i < 256; i++)
    {
      tab[i] = 0;
    }
  
  if (info=='c')
    {
      for (int i=0; i < nH; i++)
	{
	  tab[ImgIn[i*nW+numero]] =  tab[ImgIn[i*nW+numero]] + 1;
	}
    }
  
  else if (info=='l')
    {
      for (int j=0; j < nW; j++)
	{
	  tab[ImgIn[numero*nW+j]] =  tab[ImgIn[numero*nW+j]] + 1;
	}
    }
    

  for (int k=0; k < 256; k++)
    {
      printf("%d %d\n", k, tab[k]);
    }

  FILE* fichier = NULL;
  fichier = fopen("profil.dat","w");
  if (fichier!=NULL)
    {
      for(int i=0; i < 256; i++) {
	fprintf(fichier, "%d %d\n", i, tab[i]);
	  }
      
      fclose(fichier);
    }
  else
    {
      printf("Impossible d'ouvrir le fichier profil.dat");
    }
  
  free(ImgIn);
  return 1;
}
