1// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[255], cNomImgEcrite[255];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   //sscanf (argv[3],"%d",&S);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }
   
   /*
   //cas du pixel en haut a gauche
   if (ImgIn[0*nW+0] == 255)
     {
       if (( ImgIn[0*nW+1] == 0 ) or ( ImgIn[1*nW+0] == 0 ) or ( ImgIn[1*nW+1] == 0 ))
				   { ImgOut[0*nW+0] = 0; }
       else { ImgOut[0*nW+0] = 255; } 
     }
   else { ImgOut[0*nW+0] = 255; }
				 

   //cas du pixel en haut a droite
   if (ImgIn[0*nW+(nW-1)] == 255)
     {
       if (( ImgIn[0*nW+(nW-2)] == 0 ) or ( ImgIn[1*nW+(nW-2)] == 0 ) or ( ImgIn[1*nW+(nW-1)] == 0 ))
	 { ImgOut[0*nW+(nW-1)] = 0; }
       else { ImgOut[0*nW+(nW-1)] = 255; }
     }
   else { ImgOut[0*nW+(nW-1)] = 255; }

   //cas du pixel en bas a gauche
   if (ImgIn[(nH-1)*nW+0] == 255)
     {
       if (( ImgIn[(nH-2)*nW+0] == 0 ) or ( ImgIn[(nH-2)*nW+1] == 0 ) or ( ImgIn[(nH-1)*nW+1] == 0 ))
	 { ImgOut[(nH-1)*nW+0] = 0; }
       else { ImgOut[(nH-1)*nW+0] = 255; }
     }
   else { ImgOut[(nH-1)*nW+0] = 255; }

   // cas du pixel en bas a droite
   if (ImgIn[(nH-1)*nW+(nW-1)] == 255)
     {
       if (( ImgIn[(nH-2)*nW+(nW-1)] == 0 ) or ( ImgIn[(nH-2)*nW+(nW-2)] == 0 ) or ( ImgIn[(nH-1)*nW+(nW-2)] == 0 ))
	 { ImgOut[(nH-1)*nW+(nW-1)] = 0; }
       else { ImgOut[(nH-1)*nW+(nW-1)] = 255; }
     }
   else { ImgOut[(nH-1)*nW+(nW-1)] = 255; }
   
   //cas de la premiere colonne
   for (int i=1; i < nH-1; i++)
     {
       if ( ImgIn[i*nW+0] == 255 )
	 {
	   if ((ImgIn[(i-1)*nW+0] == 0) or (ImgIn[(i-1)*nW+1] == 0)
	       or (ImgIn[i*nW+1] == 0)
	       or (ImgIn[(i+1)*nW+1] == 0) or (ImgIn[(i+1)*nW+0] == 0))
	       { ImgOut[i*nW+0] = 0;}
	   else { ImgOut[i*nW+0] = 255; }
	 }
       else { ImgOut[i*nW+0] = 255; }
     }

   //cas de la premiere ligne
   for (int j=1; j < nW-1; j++)
     {
       if ( ImgIn[0*nW+j] == 255 )
	 {
	   if ((ImgIn[0*nW+(j-1)] == 0) or (ImgIn[1*nW+(j-1)] == 0)
	       or (ImgIn[1*nW+j] == 0)
	       or (ImgIn[1*nW+(j+1)] == 0) or (ImgIn[0*nW+(j+1)] == 0))
	     { ImgOut[0*nW+j] = 0; }
	   else { ImgOut[0*nW+j] = 255; }
	 }
       else { ImgOut[0*nW+j] = 255; }
     }

   //cas de la derniere colonne
   for (int i=1; i<nH-1; i++)
     {
       if ( ImgIn[i*nW+(nW-1)] == 255 )
	 {
	   if ((ImgIn[(i-1)*nW+(nW-1)] == 0) or (ImgIn[(i-1)*nW+(nW-2)] == 0)
	       or (ImgIn[i*nW+(nW-2)] == 0)
	       or (ImgIn[(i+1)*nW+(nW-2)] == 0) or (ImgIn[(i+1)*nW+(nW-1)] == 0))
	     { ImgOut[i*nW+(nW-1)] = 0; }
	   else { ImgOut[i*nW+(nW-1)] = 255; }
	 }
       else { ImgOut[i*nW+(nW-1)] = 255; }
     }

   //cas de la derniere ligne
   for (int j=1; j<nW-1; j++)
     {
       if ( ImgIn[(nH-1)*nW+j] == 255 )
	 {
	   if ((ImgIn[(nH-1)*nW+(j-1)] == 0) or (ImgIn[(nH-2)*nW+(j-1)] == 0)
	       or (ImgIn[(nH-2)*nW+j] == 0)
	       or (ImgIn[(nH-2)*nW+(j+1)] == 0) or (ImgIn[(nH-1)*nW+(j+1)]))
	     { ImgOut[(nH-1)*nW+j] = 0; }
	   else { ImgOut[(nH-1)*nW+j] = 255; }
	 }
       else { ImgOut[(nH-1)*nW+j] = 255; }
     }
   */
   
   //cas de toute l'image sauf les bordures 
   for (int i=1; i < nH-1; i++)
     for (int j=1; j < nW-1; j++)
       {
	 if ( ImgIn[i*nW+j] == 0 )
	   {
	     if ((ImgIn[(i-1)*nW+(j-1)] == 255) or (ImgIn[(i-1)*nW+j] == 255) or (ImgIn[(i-1)*nW+(j+1)] == 255)
		 or (ImgIn[i*nW+(j-1)] == 255) or (ImgIn[i*nW+(j+1)] == 255)
		 or (ImgIn[(i+1)*nW+(j-1)] == 255) or (ImgIn[(i+1)*nW+j] == 255) or (ImgIn[(i+1)*nW+(j+1)] == 255))
	       { ImgOut[i*nW+j] = 255; }
	   }
	 else { ImgOut[i*nW+j] = 255; }
       }
 
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
