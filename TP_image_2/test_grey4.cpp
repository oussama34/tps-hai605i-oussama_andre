#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  
  char cNomImgLue[255], cNomImgEcrite[255];
  int nH, nW, nTaille, nR, nG, nB, S_R, S_G, S_B;
  
  if (argc != 6) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm Seuil1 Seuil2 Seuil3 \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&S_R);
   sscanf (argv[4],"%d",&S_G);
   sscanf (argv[5],"%d",&S_B);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);
   
   // Lire l'image en couleur dans ImgIn
   OCTET *imgRouge, *imgVert, *imgBleu;
   allocation_tableau(imgRouge, OCTET, nTaille); 
   allocation_tableau(imgVert, OCTET, nTaille);  
   allocation_tableau(imgBleu, OCTET, nTaille);
   planR(imgRouge, ImgIn, nTaille);  
   planV(imgVert, ImgIn, nTaille); 
   planB(imgBleu, ImgIn, nTaille); 

   // Utilisation des imgRouge, imgVert et imgBleu comme n'importe quelle image en niveau de gris
   

   // Remettre chaque composante rouge, verte, bleue dans une image en couleur, pour l'enregistrer
   for (int i = 0; i < nTaille3; i += 3) {
     ImgOut[i] = imgRouge[i];
     ImgOut[i+1] = imgVert[i+1];
     ImgOut[i+2] = imgBleu[i+2];
   }
   
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(imgRouge); free(imgVert); free(imgBleu);
   free(ImgIn); free(ImgOut);

   return 1;
}
