#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite1[256], cNomImgEcrite2[256], cNomImgEcrite3[256];
  int nH, nW, nTaille;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageOutY.pgm ImageOutCr.pgm ImageOutCr.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite1);
   sscanf (argv[3],"%s",cNomImgEcrite2);
   sscanf (argv[4],"%s",cNomImgEcrite3);

   OCTET *ImgIn, *ImgInR, *ImgInG, *ImgInB, *ImgOutY, *ImgOutCb, *ImgOutCr;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;

   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOutY, OCTET, nTaille);
   allocation_tableau(ImgOutCb, OCTET, nTaille);
   allocation_tableau(ImgOutCr, OCTET, nTaille);

   allocation_tableau(ImgInR, OCTET, nTaille);
   allocation_tableau(ImgInG, OCTET, nTaille);
   allocation_tableau(ImgInB, OCTET, nTaille);
   planR(ImgInR, ImgIn, nTaille);
   planV(ImgInG, ImgIn, nTaille);
   planB(ImgInB, ImgIn, nTaille);
   
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
	 ImgOutY[i*nW+j] = 0.299*ImgInR[i*nW+j] + 0.587*ImgInG[i*nW+j] + 0.114*ImgInB[i*nW+j];
	 ImgOutCb[i*nW+j] = -0.1687*ImgInR[i*nW+j] - 0.3313*ImgInG[i*nW+j] + 0.5*ImgInB[i*nW+j] + 128; 
	 ImgOutCr[i*nW+j] = 0.5*ImgInR[i*nW+j] - 0.4187*ImgInG[i*nW+j] - 0.0813*ImgInB[i*nW+j] + 128; 
       }

   ecrire_image_pgm(cNomImgEcrite1, ImgOutY,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite2, ImgOutCb,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite3, ImgOutCr,  nH, nW);
   free(ImgIn); free(ImgInR); free(ImgInG); free(ImgInB);
   free(ImgOutY); free(ImgOutCb); free(ImgOutCr);

   return 1;
}
