#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite[256];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgTemp, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgTemp, OCTET, nTaille);
   allocation_tableau(ImgOut, OCTET, nTaille);

   // cas ou l'objet est en noir et le fond est en blanc
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
	 if ( ImgIn[i*nW+j] == 0 )
	   {
	     if ((ImgIn[(i-1)*nW+(j-1)] == 255) or  (ImgIn[(i-1)*nW+j] == 255) or (ImgIn[(i-1)*nW+(j+1)] == 255)
		 or (ImgIn[i*nW+(j-1)] == 255) or (ImgIn[i*nW+(j+1)] == 255)
		 or (ImgIn[(i+1)*nW+(j-1)] == 255) or (ImgIn[(i+1)*nW+j] == 255) or (ImgIn[(i+1)*nW+(j+1)] == 255))
	       { ImgTemp[i*nW+j] = 255; }
	     else { ImgTemp[i*nW+j] = 0; }
	   }
	 else { ImgTemp[i*nW+j] = 255; }
       }

   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
	 if ( ImgTemp[i*nW+j] == 255 )
	   {
	     if ((ImgTemp[(i-1)*nW+(j-1)] == 0) or  (ImgTemp[(i-1)*nW+j] == 0) or (ImgTemp[(i-1)*nW+(j+1)] == 0)
		 or (ImgTemp[i*nW+(j-1)] == 0) or (ImgTemp[i*nW+(j+1)] == 0)
		 or (ImgTemp[(i+1)*nW+(j-1)] == 0) or (ImgTemp[(i+1)*nW+j] == 0) or (ImgTemp[(i+1)*nW+(j+1)] == 0))
	       { ImgOut[i*nW+j] = 0; }
	     else { ImgOut[i*nW+j] = 255; }
	   }
	 else { ImgOut[i*nW+j] = 0; }
       }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgTemp); free(ImgOut);

   return 1;
}
