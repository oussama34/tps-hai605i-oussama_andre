#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite[256];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgInR, *ImgInG, *ImgInB, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;

   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   allocation_tableau(ImgInR, OCTET, nTaille);
   allocation_tableau(ImgInG, OCTET, nTaille);
   allocation_tableau(ImgInB, OCTET, nTaille);
   planR(ImgInR, ImgIn, nTaille);
   planV(ImgInG, ImgIn, nTaille);
   planB(ImgInB, ImgIn, nTaille);
   
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
	 ImgOut[i*nW+j] = 0.299*ImgInR[i*nW+j] + 0.587*ImgInG[i*nW+j] + 0.114*ImgInB[i*nW+j];	 
       }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgInR); free(ImgInG); free(ImgInB); free(ImgOut);

   return 1;
}
