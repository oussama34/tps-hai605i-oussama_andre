#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue1[256], cNomImgLue2[256], cNomImgLue3[256], cNomImgEcrite[256];
  int nH, nW, nTaille;
  
  if (argc != 5) 
     {
       printf("Usage: ImageInY.pgm ImageInCb.pgm ImageInCr.pgm ImgOut.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue1) ;
   sscanf (argv[2],"%s",cNomImgLue2);
   sscanf (argv[3],"%s",cNomImgLue3);
   sscanf (argv[4],"%s",cNomImgEcrite);

   OCTET *ImgInY, *ImgInCb, *ImgInCr, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
   nTaille = nH * nW;

   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgInY, OCTET, nTaille);
   lire_image_pgm(cNomImgLue1, ImgInY, nH * nW);
   allocation_tableau(ImgInCb, OCTET, nTaille);
   lire_image_pgm(cNomImgLue2, ImgInCb, nH * nW);
   allocation_tableau(ImgInCr, OCTET, nTaille);
   lire_image_pgm(cNomImgLue3, ImgInCr, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);

   int k = 0;
   for (int i = 0; i < nH; i++)
     for (int j = 0; j < nW; j++)
       {
	 ImgOut[k] = ImgInY[i*nW+j];
	 ImgOut[k+1] = ImgInCb[i*nW+j];
	 ImgOut[k+2] = ImgInCr[i*nW+j];
	 k += 3;
       }
   
   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgInY); free(ImgInCb); free(ImgInCr); free(ImgOut);

   return 1;
}

   
