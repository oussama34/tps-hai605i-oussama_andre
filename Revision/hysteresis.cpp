#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite[256];
  int nH, nW, nTaille, SH, SB;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm SB SH \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&SB);
   sscanf (argv[4],"%d",&SH);

   OCTET *ImgIn, *ImgTemp, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgTemp, OCTET, nTaille);
   allocation_tableau(ImgOut, OCTET, nTaille);

   // premiere lecture
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
	 if (ImgIn[i*nW+j] <= SB) { ImgTemp[i*nW+j] = 0; }
	 else
	   {
	     if (ImgIn[i*nW+j] >= SH) { ImgTemp[i*nW+j] = 255; }
	     else ImgTemp[i*nW+j] = ImgIn[i*nW+j];
	   }
       }

   // deuxieme lecture
   for (int i=1; i < nH-1; i++)
     for (int j=1; j < nW-1; j++)
       {
	 if ((SB < ImgTemp[i*nW+j]) and (ImgTemp[i*nW+j] < SH) and
	     ((ImgTemp[(i-1)*nW+(j-1)] == 255) or (ImgTemp[(i-1)*nW+j] == 255) or (ImgTemp[(i-1)*nW+(j+1)] == 255)
	      or (ImgTemp[i*nW+(j-1)] == 255)  or (ImgTemp[i*nW+(j+1)] == 255) or
	      (ImgTemp[(i+1)*nW+(j-1)] == 255) or (ImgTemp[(i+1)*nW+j] == 255) or (ImgTemp[(i+1)*nW+(j+1)] == 255)))
	   { ImgOut[i*nW+j] = 255; }
	 else { ImgOut[i*nW+j] = 0; }
       }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgTemp); free(ImgOut);

   return 1;
}
