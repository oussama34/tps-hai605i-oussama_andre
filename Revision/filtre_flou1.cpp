#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite[256];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   // gestion des bordures
   for (int i = 0; i < nH; i++)
     {
       ImgOut[i*nW+0] = ImgIn[i*nW+0];
       ImgOut[i*nW+(nH-1)] = ImgIn[i*nW+(nH-1)];
       ImgOut[0*nW+i] = ImgIn[0*nW+i];
       ImgOut[(nW-1)*nW+i] = ImgIn[(nW-1)*nW+i];
     }

   // application du filtre_flou1 sur le reste de l'image
   for (int i=1; i < nH-1; i++)
     for (int j=1; j < nW-1; j++)
       {
	 ImgOut[i*nW+j] = ( ImgIn[(i-1)*nW+j] + ImgIn[i*nW+(j-1)] + ImgIn[i*nW+j] + ImgIn[i*nW+(j+1)] + ImgIn[(i+1)*nW+j] ) / 5;
       }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
