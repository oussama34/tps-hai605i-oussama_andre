#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite[256];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgInR, *ImgInG, *ImgInB, *ImgOutR, *ImgOutG, *ImgOutB, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;

   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);

   allocation_tableau(ImgInR, OCTET, nTaille);
   allocation_tableau(ImgInG, OCTET, nTaille);
   allocation_tableau(ImgInB, OCTET, nTaille);
   planR(ImgInR, ImgIn, nTaille);
   planV(ImgInG, ImgIn, nTaille);
   planB(ImgInB, ImgIn, nTaille);
   allocation_tableau(ImgOutR, OCTET, nTaille);
   allocation_tableau(ImgOutG, OCTET, nTaille);
   allocation_tableau(ImgOutB, OCTET, nTaille);

   /*
   // gestion des bordures
   for (int i = 0; i < nH; i++)
     {
       ImgOut[i*nW+0] = ImgIn[i*nW+0];
       ImgOut[i*nW+(nH-1)] = ImgIn[i*nW+(nH-1)];
       ImgOut[0*nW+i] = ImgIn[0*nW+i];
       ImgOut[(nW-1)*nW+i] = ImgIn[(nW-1)*nW+i];
     }
   */

   // application du filtre_flou2 sur le reste de l'image
   for (int i=1; i < nH-1; i++)
     for (int j=1; j < nW-1; j++)
       {
	 ImgOutR[i*nW+j] = ( ImgInR[(i-1)*nW+(j-1)] + ImgInR[(i-1)*nW+j] + ImgInR[(i-1)*nW+(j+1)] +
			    ImgInR[i*nW+(j-1)] + ImgInR[i*nW+j] + ImgInR[i*nW+(j+1)] +
			    ImgInR[(i+1)*nW+(j-1)] + ImgInR[(i+1)*nW+j] + ImgInR[(i+1)*nW+(j+1)] ) / 9;
	 
	 ImgOutG[i*nW+j] = ( ImgInG[(i-1)*nW+(j-1)] + ImgInG[(i-1)*nW+j] + ImgInG[(i-1)*nW+(j+1)] +
			    ImgInG[i*nW+(j-1)] + ImgInG[i*nW+j] + ImgInG[i*nW+(j+1)] +
			    ImgInG[(i+1)*nW+(j-1)] + ImgInG[(i+1)*nW+j] + ImgInG[(i+1)*nW+(j+1)] ) / 9;

	 ImgOutB[i*nW+j] = ( ImgInB[(i-1)*nW+(j-1)] + ImgInB[(i-1)*nW+j] + ImgInB[(i-1)*nW+(j+1)] +
			    ImgInB[i*nW+(j-1)] + ImgInB[i*nW+j] + ImgInB[i*nW+(j+1)] +
			    ImgInB[(i+1)*nW+(j-1)] + ImgInB[(i+1)*nW+j] + ImgInB[(i+1)*nW+(j+1)] ) / 9;
       }
   
   int j = 0;
   for (int i = 0; i < nTaille3; i += 3)
     {
       ImgOut[i] = ImgOutR[j];
       ImgOut[i+1] = ImgOutG[j];
       ImgOut[i+2] = ImgOutB[j];
       j++;
     }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgInR); free(ImgInG); free(ImgInB); free(ImgOutR); free(ImgOutG); free(ImgOutB); free(ImgOut);

   return 1;
}
