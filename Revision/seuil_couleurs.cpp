// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include <stdlibl.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite[256];
  int nH, nW, nTaille, S_R, S_G, S_B;
  
  if (argc != 6) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm S_R S_G S_B \n"); 
       exit(1);
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&S_R);
   sscanf (argv[4],"%d",&S_G);
   sscanf (argv[5],"%d",&S_B);

   OCTET *ImgIn, *ImgInR, *ImgInG, *ImgInB, *ImgOutR, *ImgOutG, *ImgOutB, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);

   allocation_tableau(ImgInR, OCTET, nTaille);
   allocation_tableau(ImgInG, OCTET, nTaille);
   allocation_tableau(ImgInB, OCTET, nTaille);
   planR(ImgInR, ImgIn, nTaille);
   planV(ImgInG, ImgIn, nTaille);
   planB(ImgInB, ImgIn, nTaille);
   allocation_tableau(ImgOutR, OCTET, nTaille);
   allocation_tableau(ImgOutG, OCTET, nTaille);
   allocation_tableau(ImgOutB, OCTET, nTaille);

   /*
   for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       if (nR < S) ImgOut[i]=0; else ImgOut[i]=255;
       if (nG < S) ImgOut[i+1]=0; else ImgOut[i+1]=255; 
       if (nB < S) ImgOut[i+2]=0; else ImgOut[i+2]=255;
     }
   */

   for (int i = 0; i < nH; i++) {
     for (int j = 0; j < nW; j++) {
       if (ImgInR[i*nW+j] < S_R) { ImgOutR[i*nW+j] = 0;}
       else { if (ImgInR[i*nW+j] >= S_R) { ImgOutR[i*nW+j] = 255; }}
       if (ImgInG[i*nW+j] < S_G) { ImgOutG[i*nW+j] = 0;}
       else { if (ImgInG[i*nW+j] >= S_G) { ImgOutG[i*nW+j] = 255; }}
       if (ImgInB[i*nW+j] < S_B) { ImgOutB[i*nW+j] = 0;}
       else { if (ImgInB[i*nW+j] >= S_B) { ImgOutB[i*nW+j] = 255; }}
     }
   }

   int j = 0;
   for (int i = 0; i < nTaille3; i += 3)
     {
       ImgOut[i] = ImgOutR[j];
       ImgOut[i+1] = ImgOutG[j];
       ImgOut[i+2] = ImgOutB[j];
       j++;
     }
   
   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgInR); free(ImgInG); free(ImgInB); free(ImgOutR); free(ImgOutG); free(ImgOutB); free(ImgOut);

   return 1;
}
