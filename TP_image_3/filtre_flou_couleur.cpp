// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite[256];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);

   // Lire l'image en couleur dans ImgIn
   OCTET *imgRouge, *imgVert, *imgBleu;
   allocation_tableau(imgRouge, OCTET, nTaille);
   allocation_tableau(imgVert, OCTET, nTaille);
   allocation_tableau(imgBleu, OCTET, nTaille);
   planR(imgRouge, ImgIn, nTaille);
   planV(imgVert, ImgIn, nTaille);
   planB(imgBleu, ImgIn, nTaille);

   // Utilisation des imgRouge, imgVert et imgBleu comme n'importe quelle image en niveau de gris
   for (int i=1; i < nH-1; i++)
     for (int j = 1; j < nW-1; j++)
       {
	 imgRouge[i*nW+j] = (ImgIn[(i-1)*nW+(j-1)]+ImgIn[(i-1)*nW+j]+ImgIn[(i-1)*nW+(j+1)]+
			 ImgIn[i*nW+(j-1)]+ImgIn[i*nW+j]+ImgIn[i*nW+(j+1)]+
			 ImgIn[(i+1)*nW+(j-1)]+ImgIn[(i+1)*nW+j]+ImgIn[(i+1)*nW+(j+1)]
			 )/9;
	 imgVert[i*nW+j] = (ImgIn[(i-1)*nW+(j-1)]+ImgIn[(i-1)*nW+j]+ImgIn[(i-1)*nW+(j+1)]+
			 ImgIn[i*nW+(j-1)]+ImgIn[i*nW+j]+ImgIn[i*nW+(j+1)]+
			 ImgIn[(i+1)*nW+(j-1)]+ImgIn[(i+1)*nW+j]+ImgIn[(i+1)*nW+(j+1)]
			 )/9;
	 imgBleu[i*nW+j] = (ImgIn[(i-1)*nW+(j-1)]+ImgIn[(i-1)*nW+j]+ImgIn[(i-1)*nW+(j+1)]+
			 ImgIn[i*nW+(j-1)]+ImgIn[i*nW+j]+ImgIn[i*nW+(j+1)]+
			 ImgIn[(i+1)*nW+(j-1)]+ImgIn[(i+1)*nW+j]+ImgIn[(i+1)*nW+(j+1)]
			 )/9;
       }

   // Remettre chaque composante rouge, verte, bleue dans une image en couleur, pour l'enregistrer
   for (int i = 0; i < nTaille3; i += 3) {
     ImgOut[i] = imgRouge[i/3];
     ImgOut[i+1] = imgVert[i/3];
     ImgOut[i+2] = imgBleu[i/3];
   }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(imgRouge); free(imgVert); free(imgBleu);
   free(ImgIn); free(ImgOut);
   return 1;
}
