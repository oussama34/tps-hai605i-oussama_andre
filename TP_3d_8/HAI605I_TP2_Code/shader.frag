// --------------------------------------------------------------------------
// gMini,
// a minimal Glut/OpenGL app to extend                              
//
// Copyright(C) 2007-2009                
// Tamy Boubekeur
//                                                                            
// All rights reserved.                                                       
//                                                                            
// This program is free software; you can redistribute it and/or modify       
// it under the terms of the GNU General Public License as published by       
// the Free Software Foundation; either version 2 of the License, or          
// (at your option) any later version.                                        
//                                                                            
// This program is distributed in the hope that it will be useful,            
// but WITHOUT ANY WARRANTY; without even the implied warranty of             
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
// GNU General Public License (http://www.gnu.org/licenses/gpl.txt)           
// for more details.                                                          
//                                                                          
// --------------------------------------------------------------------------

uniform float ambientRef;
uniform float diffuseRef;
uniform float specRef;
uniform float shininess;

varying vec4 p;
varying vec3 n;

void main (void) {
     vec3 P = vec3 (gl_ModelViewMatrix * p); //Position du point à éclairer
     vec3 N = normalize (gl_NormalMatrix * n); //Normal en ce point
     vec3 V = normalize (-P); //Vecteur de vue
     

     vec4 lightContribution = ambientRef * gl_LightModel.ambient*gl_FrontMaterial.ambient;

     for (int i = 0; i < 3; i++)
     {
	vec3 L = normalize(gl_LightSource[i].position.xyz - P);
     	vec3 R = reflect(-L,N);

	lightContribution += (diffuseRef * gl_LightSource[i].diffuse*gl_FrontMaterial.diffuse * max(0.0,dot(L,N)));

    	lightContribution += (specRef * gl_LightSource[i].specular*gl_FrontMaterial.specular * pow(max(0.0,dot(R,V)), shininess));
    }
    
    gl_FragColor =vec4 (lightContribution.xyz, 1);
}
 
