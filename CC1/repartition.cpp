#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], nomFichier[50];
  int nH, nW, nTaille;
  if (argc != 3)
    {
      printf("Usage: ImgIn.pgm repartition.dat\n");
      exit(1);
    }

  sscanf(argv[1],"%s", cNomImgLue);
  sscanf(argv[2],"%s", nomFichier);
  

  OCTET *ImgIn;

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;

  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

  float tab[256];
  for (int i = 0; i < 256; i++) {
    tab[i] = 0;
  }

  for (int i = 0; i < nH; i++) {
    for (int j = 0; j < nW; j++) {
      tab[ImgIn[i*nW+j]] = tab[ImgIn[i*nW+j]] + 1;
    }
  }

  for (int i = 0; i < 256; i++)
    {
      tab[i] = (float)tab[i] / (float)(nH*nW);
    }

  float F[256];
  for (int i = 0; i < 256; i++)
    {
      F[i] = 0;
    }

  F[0] = tab[0];
  for (int i = 1; i < 256; i++)
    {
      F[i] = F[i-1] + tab[i];
    }

  FILE* fichier = NULL;
  fichier = fopen(nomFichier, "w");
  if (fichier != NULL) {
    for (int i = 0; i < 256; i++) {
      fprintf(fichier, "%d %f\n", i, F[i]);
    }
    fclose(fichier);
  }
  else {
    printf("Impossible d'ouvrir le fichier histo.dat");
  }

  free(ImgIn);

  return 1;
}
