#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[256], cNomImgEcrite[256];
  int nH, nW, nTaille;
  if (argc != 3)
    {
      printf("Usage: ImgIn.pgm ImgOut.pgm \n");
      exit(1);
    }

  sscanf(argv[1],"%s", cNomImgLue);
  sscanf(argv[2],"%s", cNomImgEcrite);
  

  OCTET *ImgIn, *ImgOut;

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;

  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut, OCTET, nTaille);

  float tab[256];
  for (int i = 0; i < 256; i++) {
    tab[i] = 0;
  }

  for (int i = 0; i < nH; i++) {
    for (int j = 0; j < nW; j++) {
      tab[ImgIn[i*nW+j]] = tab[ImgIn[i*nW+j]] + 1;
    }
  }

  float ddp[256];
  for (int i = 0; i < 256; i++) {
    ddp[i] = 0;
  }
  
  for (int i = 0; i < 256; i++)
    {
      ddp[i] = (float)tab[i] / (float)(nH*nW);
    }

  float F[256];
  for (int i = 0; i < 256; i++)
    {
      F[i] = 0;
    }

  F[0] = ddp[0];
  for (int i = 1; i < 256; i++)
    {
      F[i] = F[i-1] + ddp[i];
    }

  for (int i = 0; i < nH; i++)
    for (int j = 0; j < nW; j++)
      {
	ImgOut[i*nW+j] = floor(F[ImgIn[i*nW+j]]*255);
      }


  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
  free(ImgIn); free(ImgOut);

  return 1;
}
